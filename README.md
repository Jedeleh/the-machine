The Machine is simply a word count tool that exists purely in the user's
browser. This is by design as it's meant to be used by writers who wish to
analyze the word use and any patterns of overuse of words in their content and
in order for it to be "trustworthy" to people who want to control their
content, it never leaves their browser.

The build setup is designed to rely on rake, coffeescript and sass. If you
don't have those things, you might want to get them before monkeying around in
here.