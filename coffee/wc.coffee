class WordCounter

  constructor: ->
    @excludes = []
    @target_words = {}
    @word_count_limit = $("#limit").val()
    @total_word_count = 0
    @word_keys = []


  exclude_words: (id) =>
    word_list = []
    switch
      when id == "exclude-articles" then word_list = WordCounter.ARTICLES_EXCLUDES
      when id == "exclude-pronouns" then word_list = WordCounter.PRONOUN_EXCLUDES
      when id == "exclude-conjunctions" then word_list = WordCounter.SIMPLE_CONJUNCTION_EXCLUDES
      when id == "exclude-prepositions" then word_list = WordCounter.PREPOSITION_EXCLUDES
      when id == "exclude-beverb" then word_list = WordCounter.BEING_VERB_EXCLUDES
    word_list

  get_canned_excludes: =>
    @excludes = []
    for exclusion in $("#excludes :checkbox:checked")
      id = $(exclusion).attr("id")
      words = @exclude_words(id)
      @excludes = @excludes.concat words

    user_excludes = $("#user-excludes").val()
    @excludes.push user_exclude.trim().toLowerCase() for user_exclude in user_excludes.split("\n")

    console.log @excludes

  load_excludes: =>
    # are we using the builtin excludes?
    if $("#use-builtin-excludes").is(":checked")
      @excludes = [
        "is", "it", "in", "the", "and", "a", "to", "but", "not", "or", "of", "at", "with", "was", "i",
        "she", "her", "my", "that", "on", "me", "for", "into", "out", "were", "you", "he", "me", "his",
        "on", "for", "him", "out", "we", "up"
      ]

    user_excludes = $("#user-excludes").val()
    @excludes.push user_exclude.trim().toLowerCase() for user_exclude in user_excludes.split("\n")

  get_user_chosen_preexcludes: =>

  process_user_content: =>
    user_content = $("#user-content").val()
    user_content = user_content.trim().replace /[  ]+/g, " "
    user_content = user_content.replace /[\.,!\"\?]/g, ""
    user_content = user_content.replace /[\u201C\u201D\u002E\u002c\uFE52\u0315]/g, ""
    @process_line line for line in user_content.split "\n"

  process_line: (line) =>
    @process_word(word.trim().toLowerCase()) for word in line.trim().split(/[ \-]/)

  process_word: (word) =>
    if word == ""
      return
    if word not in @excludes

      word_data = null
      if @target_words.hasOwnProperty(word)
        word_data = @target_words[word]
      else
        word_data = new WordData(word)
      word_data.record(@total_word_count)
      @target_words[word] = word_data
    @total_word_count += 1

  dump_words: =>
    keys = Object.keys(@target_words).sort (a, b) => @target_words[b].word_count - @target_words[a].word_count
    for key in keys
      word_data = @target_words[key]
      word_data.close()
      hotspots = word_data.hotspots()


  display_results: =>
    @clear_results()
    header_row = $("<tr><td>word count</td><td>word</td><td># hotspots</td></tr>")
    $("#results-table").append($(header_row))

    # sort the results by value (which is the count)
    keys = Object.keys(@target_words).sort (a, b) => @target_words[b].word_count - @target_words[a].word_count

    for key in keys
      word_data = @target_words[key]
      word_data.close()
      if @target_words[key].word_count >= @word_count_limit
        @add_one_result_row(key, @target_words[key])

  add_one_result_row: (word, word_data) =>
    if word_data.hotspots().length > 0
      tool_tip_text = ""
      count = 1
      for spot in word_data.hotspots()
        tool_tip_text = tool_tip_text + "hotspot #{count}: #{spot.locations.join(", ")} "
        tool_tip_text = tool_tip_text + "\n"
        count = count + 1
      tool_tip = "<td class='dyn-tooltip' data-toggle='tooltip' data-placement='right' title='#{tool_tip_text}'>#{word_data.hotspots().length}</td>"
    else
      tool_tip = "<td></td>"

    word_text = "<a target='_blank' href='http://www.thesaurus.com/browse/#{word}'>#{word}</a>"
    new_row = $("<tr><td>#{word_data.word_count}</td><td>#{word_text}</td>#{tool_tip}</tr>")
    $("#results-table").append($(new_row))
    for tt in $(".dyn-tooltip")
      $(tt).tooltip(container:'body')

  clear_results: =>
    $("#results-table tr").remove()

  @ARTICLES_EXCLUDES: [
    "a", "the", "an",
  ]
  @PRONOUN_EXCLUDES: [
    "she", "he", "her", "him", "they", "them", "we", "us", "you"
  ]
  @SIMPLE_CONJUNCTION_EXCLUDES: [
    "and", "or", "but", "then", "when", "after", "although"
  ]
  @PREPOSITION_EXCLUDES: [
    "as", "at", "by", "for", "from", "in", "into", "like", "of", "than", "to", "with"
  ]
  @BEING_VERB_EXCLUDES: [
    "am", "are", "is", "was", "were", "being", "be", "been"
  ]

  @BASIC_EXCLUDES: @ARTICLES_EXCLUDES.concat(@PRONOUN_EXCLUDES).concat(@SIMPLE_CONJUNCTION_EXCLUDES).concat(@BEING_VERB_EXCLUDES)


class WordData
  constructor: (word_name) ->
    @word = word_name
    @word_count = 0
    @current_hotspot = null
    @found_hotspots = []
    @hotspot_counter = 0

  record: (location) =>
    # account for hotspots
    #
    if @current_hotspot == null
      @current_hotspot = new HotspotData(@word)

    if @current_hotspot.check(location)
      @current_hotspot.record(location)
    else
      if @current_hotspot.close()
        @found_hotspots.push @current_hotspot
        @current_hotspot = new HotspotData(@word)
        @current_hotspot.record(location)
    @word_count += 1

  hotspots: =>
    @found_hotspots

  close: =>
    if @current_hotspot != null && @current_hotspot.close()
      @found_hotspots.push @current_hotspot
      @current_hotspot = new HotspotData(@word)

class HotspotData
  constructor: (word_name) ->
    @word = word_name
    @locations = []
    @delta_threshold = 30
    @location_threshold = 3

  check: (location) =>
    check_passed = false
    if @locations.length >= 1
      [first,...,last] = @locations
      delta = location - last
      check_passed = delta <= @delta_threshold
    else
      check_passed = true

    check_passed

  record: (word_location) =>
    @locations.push word_location

  close: =>
    create_new = false
    if @locations.length < @location_threshold
      @locations = []
    else
      create_new = true

    create_new

  locations: =>
    @locations



start = () ->
  word_counter = new WordCounter()
  #word_counter.load_excludes()
  word_counter.get_canned_excludes()
  word_counter.process_user_content()
  word_counter.display_results()
  $("#basicModal").modal("show")
# word_counter.dump_words()

